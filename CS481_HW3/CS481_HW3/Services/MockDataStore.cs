﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CS481_HW3.Models;

namespace CS481_HW3.Services
{
    public class MockDataStore : IDataStore<Item>
    {
        readonly List<Item> items;

        public MockDataStore()
        {
            // DEFINE ALL POKEMON HERE
            // NOTE : WILL IMPROVE BY CALLING AN API IN FUTURE HOMEWORK
            items = new List<Item>()
            {
                new Item { Id = Guid.NewGuid().ToString(), Text = "Bulbasaur", Description = "It evolves into Ivysaur starting at level 16, which evolves into Venusaur starting at level 32.", Image="https://assets.pokemon.com/assets/cms2/img/pokedex/full/001.png" },
                new Item { Id = Guid.NewGuid().ToString(), Text = "Ivysaur", Description = "It evolves from Bulbasaur starting at level 16 and evolves into Venusaur starting at level 32.", Image="https://assets.pokemon.com/assets/cms2/img/pokedex/full/002.png" },
                new Item { Id = Guid.NewGuid().ToString(), Text = "Venusaur", Description = "It evolves from Ivysaur starting at level 32. It is the final form of Bulbasaur. It can Mega Evolve into Mega Venusaur using the Venusaurite. It has a Gigantamax form that will appear in The Isle of Armor. Venusaur is the game mascot of both Green and LeafGreen, appearing on the boxart of both.", Image="https://assets.pokemon.com/assets/cms2/img/pokedex/full/003.png" },
                new Item { Id = Guid.NewGuid().ToString(), Text = "Charmander", Description = "It evolves into Charmeleon starting at level 16, which evolves into Charizard starting at level 36.", Image="https://assets.pokemon.com/assets/cms2/img/pokedex/full/004.png" },
                new Item { Id = Guid.NewGuid().ToString(), Text = "Charmeleon", Description = "It evolves from Charmander starting at level 16 and evolves into Charizard starting at level 36.", Image="https://assets.pokemon.com/assets/cms2/img/pokedex/full/005.png" },
                new Item { Id = Guid.NewGuid().ToString(), Text = "Charizard", Description = "It evolves from Charmeleon starting at level 36. It is the final form of Charmander. It can Mega Evolve into two forms: Mega Charizard X using Charizardite X and Mega Charizard Y using Charizardite Y. It has a Gigantamax form. Charizard is the game mascot of Pokémon Red and FireRed Versions.", Image="https://assets.pokemon.com/assets/cms2/img/pokedex/full/006.png" },
            };
        }

        public async Task<bool> AddItemAsync(Item item)
        {
            items.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> UpdateItemAsync(Item item)
        {
            var oldItem = items.Where((Item arg) => arg.Id == item.Id).FirstOrDefault();
            items.Remove(oldItem);
            items.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> DeleteItemAsync(string id)
        {
            var oldItem = items.Where((Item arg) => arg.Id == id).FirstOrDefault();
            items.Remove(oldItem);

            return await Task.FromResult(true);
        }

        public async Task<Item> GetItemAsync(string id)
        {
            return await Task.FromResult(items.FirstOrDefault(s => s.Id == id));
        }

        public async Task<IEnumerable<Item>> GetItemsAsync(bool forceRefresh = false)
        {
            return await Task.FromResult(items);
        }
    }
}